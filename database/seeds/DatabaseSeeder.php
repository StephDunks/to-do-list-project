<?php

use Illuminate\Database\Seeder;
use App\Models\Todos;
use Faker\Factory;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $faker = Factory::create();

       $todo = new Todos();
       $todo->name = $faker->name;
       $todo->content = $faker->text(30);
       $todo->save();
    }
}
